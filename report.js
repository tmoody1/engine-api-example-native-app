setInterval(function () {
	api.config.credentials.username = $("#user").val();
	api.config.credentials.password = $("#pw").val();
	api.config.siteId = $("a.active").attr("value");
}, 20);

window.index = 0;
window.indexAll = 0;
window.inProgress = false;
window.inProgressAll = false;
window.rateLimit = 10000;
window.delay = 3000; // 3 Seconds
window.downloadProgress = false;
window.downloadIndex = 0;

var api = {
	"config" : {
		"siteId" : 0,
		"server" : "https://admin1.sitespect.com",
		"credentials" : {
			"username" : "",
			"password" : ""
		}
	},
	"reports" : {},
	"segments" : {
		"615" : [{
				"name" : "Global",
				"id" : "none"
			}
		],
		"450" : [{
				"name" : "Global",
				"id" : "none"
			}
		],
		"591" : [{
				"name" : "Global",
				"id" : "none"
			}
		],
		"351" : [{
				"name" : "Global",
				"id" : "none"
			}, {
				"name" : "User Type - Gift Giver",
				"id" : "10219"
			}, {
				"name" : "User Type - Registrant",
				"id" : "10218"
			}, {
				"name" : "User Type - Shopper",
				"id" : "10220"
			}, {
				"name" : "User Type: New Users (First Visit)",
				"id" : "9022"
			}, {
				"name" : "User Type: Returning Users",
				"id" : "9023"
			}, {
				"name" : "Browser: Chrome",
				"id" : "9016"
			}, {
				"name" : "Browser: IE (All versions)",
				"id" : "9008"
			}, {
				"name" : "Browser: Opera",
				"id" : "9018"
			}, {
				"name" : "Browser: Safari",
				"id" : "9017"
			}, {
				"name" : "Add to Carts less than 10",
				"id" : "15311"
			}, {
				"name" : "Add to Registries <30",
				"id" : "15217"
			}, {
				"name" : "Orders < $1000",
				"id" : "15276"
			}
		],
		"451" : [{
				"name" : "Global",
				"id" : "none"
			}, {
				"name" : "User Type - Gift Giver",
				"id" : "15255"
			}, {
				"name" : "User Type - Registrant",
				"id" : "15254"
			}, {
				"name" : "User Type - Shopper",
				"id" : "15253"
			}, {
				"name" : "User Type: New Users (First Visit)",
				"id" : "13700"
			}, {
				"name" : "User Type: Returning Users",
				"id" : "13701"
			}, {
				"name" : "Browser: Chrome",
				"id" : "13692"
			}, {
				"name" : "Browser: IE (All versions)",
				"id" : "13684"
			}, {
				"name" : "Browser: Opera",
				"id" : "13694"
			}, {
				"name" : "Browser: Safari",
				"id" : "13693"
			}, {
				"name" : "Add to Carts less than 10",
				"id" : "15326"
			}, {
				"name" : "Add to Registries <30",
				"id" : "15327"
			}
		],
		"350" : [{
				"name" : "Global",
				"id" : "none"
			}, {
				"name" : "User Type - Gift Giver",
				"id" : "10872"
			}, {
				"name" : "User Type - Registrant",
				"id" : "10871"
			}, {
				"name" : "User Type - Shopper",
				"id" : "10873"
			}, {
				"name" : "User Type: New Users (First Visit)",
				"id" : "8996"
			}, {
				"name" : "User Type: Returning Users",
				"id" : "8997"
			}, {
				"name" : "Browser: Chrome",
				"id" : "8990"
			}, {
				"name" : "Browser: IE (All versions)",
				"id" : "8982"
			}, {
				"name" : "Browser: Opera",
				"id" : "8992"
			}, {
				"name" : "Browser: Safari",
				"id" : "8991"
			}, {
				"name" : "Add to Carts less than 10",
				"id" : "15275"
			}, {
				"name" : "Add to Registries <30",
				"id" : "15312"
			}
		],
		"450" : [{
				"name" : "Global",
				"id" : "none"
			}, {
				"name" : "User Type - Gift Giver",
				"id" : "15252"
			}, {
				"name" : "User Type - Registrant",
				"id" : "15251"
			}, {
				"name" : "User Type - Shopper",
				"id" : "15250"
			}, {
				"name" : "User Type: New Users (First Visit)",
				"id" : "13673"
			}, {
				"name" : "User Type: Returning Users",
				"id" : "13674"
			}, {
				"name" : "Browser: Chrome",
				"id" : "13665"
			}, {
				"name" : "Browser: IE (All versions)",
				"id" : "13657"
			}, {
				"name" : "Browser: Opera",
				"id" : "13667"
			}, {
				"name" : "Browser: Safari",
				"id" : "13666"
			}, {
				"name" : "Add to Carts less than 10",
				"id" : "15246"
			}, {
				"name" : "Add to Registries <50",
				"id" : "15248"
			}
		],
		"256" : [{
				"name" : "Global",
				"id" : "none"
			}, {
				"name" : "User Type - Gift Giver",
				"id" : "6912"
			}, {
				"name" : "User Type - Registrant",
				"id" : "6913"
			}, {
				"name" : "User Type - Shopper",
				"id" : "6914"
			}, {
				"name" : "User Type: New Users (First Visit)",
				"id" : "5696"
			}, {
				"name" : "User Type: Returning Users",
				"id" : "5697"
			}, {
				"name" : "Browser: Chrome",
				"id" : "5690"
			}, {
				"name" : "Browser: IE (All versions)",
				"id" : "5683"
			}, {
				"name" : "Browser: Opera",
				"id" : "5692"
			}, {
				"name" : "Browser: Safari",
				"id" : "5691"
			}, {
				"name" : "Order Value <$1000",
				"id" : "14967"
			}, {
				"name" : "Orders < 3",
				"id" : "15801"
			}, {
				"name" : "Add to Carts less than 10",
				"id" : "7404"
			}, {
				"name" : "Add to Registries <50",
				"id" : "15152"
			}
		],
		"257" : [{
				"name" : "Global",
				"id" : "none"
			}, {
				"name" : "User Type - Gift Giver",
				"id" : "8955"
			}, {
				"name" : "User Type - Registrant",
				"id" : "8954"
			}, {
				"name" : "User Type - Shopper",
				"id" : "8956"
			}, {
				"name" : "User Type: New Users (First Visit)",
				"id" : "5720"
			}, {
				"name" : "User Type: Returning Users",
				"id" : "5721"
			}, {
				"name" : "Browser: Chrome",
				"id" : "5714"
			}, {
				"name" : "Browser: IE (All versions)",
				"id" : "5707"
			}, {
				"name" : "Browser: Opera",
				"id" : "5716"
			}, {
				"name" : "Browser: Safari",
				"id" : "5715"
			}, {
				"name" : "Order Value <$1000",
				"id" : "14979"
			}, {
				"name" : "Add to Carts less than 10",
				"id" : "15246"
			}, {
				"name" : "Add to Registries <50",
				"id" : "15245"
			}
		]
	},
	"currentToken" : "",
	"getToken" : function (callback) {

		var token;

		$.ajax({
			type : "POST",
			url : api.config.server + "/api/token",
			data : JSON.stringify(api.config.credentials),
			success : function (data) {
				console.log(data.token);
				callback(data.token);
				api.currentToken = data.token;
			}
		});
	},
	"getLocalPath" : function (callback) {

		var callback = callback;

		chrome.fileSystem.chooseEntry({
			type : 'openFile'
		}, function (entry) {
			if (!entry) {
				console.log("Cancelled");
				return;
			}

			// All of Chrome API is asynchronous! Use callbacks:
			chrome.fileSystem.getDisplayPath(entry, function (path) {
				callback(path);
			});
		});
	},
	"getActiveCampaigns" : function (token) {

		var campaigns = [];

		$("table tr.newData").remove();

		$.ajax({
			type : "GET",
			url : api.config.server + "/api/site/" + api.config.siteId + "/campaigns/active-running",
			headers : {
				"X-API-TOKEN" : token
			},
			success : function (data) {
				console.log(data._embedded.campaigns);

				for (var i = 0; i < data._embedded.campaigns.length; i++) {

					if (!(/100%|A.A|Validation/gi.test(data._embedded.campaigns[i].Name))) {
						$("tbody").append('<tr class="newData"><th scope="row campaignName" class="campaignName">' + data._embedded.campaigns[i].Name + '<td class="campaignId">' + data._embedded.campaigns[i].ID + '</td><td>-</td><td><button class="requestMatrix btn btn-primary" type="submit">Request Matrix</button><span class="loading"><img src="images/loading.gif" /></span></td><td><a class="download hidden" data-name="' + data._embedded.campaigns[i].Name + '" href="/">Download Link</a></td></th></tr>');
					}

				}

				$("a.download").click(function () {

					var linkId = $(this).attr("data");

					console.log(linkId);
					console.log(api.reports[linkId]);

					var campaignName = $(this).attr("data-name");
					console.log(campaignName);

					api.triggerDownload(api.reports[linkId].data, api.reports[linkId].name);
				});

				// bind request matrix button
				$(".requestMatrix").click(function () {
					console.log("you clicked request matrix");
					var campaignId = $(this).parents().eq(1).find(".campaignId").text();
					var campaignName = $(this).parents().eq(2).find(".campaignName").text();
					console.log("Requested Matrix for " + campaignId);

					api.getToken(function (token) {

						api.getMatrix(campaignId, campaignName, token);

					});

					$(this).hide();
					$(this).next().find("img").show();
					$(this).next().find("img").addClass("active-loading");

				});
			}
		});
	},
	"getMatrixAll" : function (token) {

		$("div.loadingAll").show();
		$("body > div > span > img").show();

		var campaigns = [];
		var campaignNames = [];

		$(".campaignId").each(function () {
			campaigns.push($(this).html());
		});

		$(".campaignName").each(function () {
			campaignNames.push($(this).html());
		});

		var eventLoop = setInterval(function () {

				if (window.inProgressAll == false && window.indexAll < campaigns.length) {

					$("tr.newData").eq(window.indexAll).find(".requestMatrix").hide();
					$("tr.newData").eq(window.indexAll).find(".requestMatrix").next().find("img").show();
					$("tr.newData").eq(window.indexAll).find(".requestMatrix").next().find("img").addClass("active-loading");

					console.log("\n\n\nStarting Next Campaign: " + campaignNames[window.indexAll]);
					api.getMatrix(campaigns[window.indexAll], campaignNames[window.indexAll], token);
					window.inProgressAll = true;
				}

				if (window.inProgressAll == false && window.indexAll == campaigns.length) {

					clearInterval(eventLoop);
					window.indexAll = 0;
					window.inProgressAll = false;
					$("div.loadingAll").hide();
					$("body > div > span > img").hide();

					console.log("get all matrix done!");

					$("#downloadAll").show();

				}
			}, 1e3);
	},
	"getMatrix" : function (campaignId, campaignName, token) {

		var totalData = "";
		var breakText = "\n\n";

		var getMatrixFunc = function () {

			if (window.inProgress == false && window.index < window.api.segments[window.api.config.siteId].length) {

				window.inProgress = true;
				console.log("Segment ID: " + api.segments[api.config.siteId][window.index].id + "\n\n");

				var segmentQuery = "&applied_segment_id=" + api.segments[api.config.siteId][window.index].id;

				if (api.segments[api.config.siteId][window.index].id == "none") {
					segmentQuery = "";
				}

				$.ajax({
					type : "GET",
					url : api.config.server + "/api/site/" + api.config.siteId + "/campaign/" + campaignId + "/campaigndata/performancematrix/visit/interactions?show_zscore=false&status=active&precision=5&show_candlesticks=false&show_confgraphic=false&show_std=false" + segmentQuery,
					headers : {
						"X-API-TOKEN" : token
					},
					timeout : 600e3, //10 minutes
					error : function () {

						window.inProgress = false;
						window.index = window.index + 1;
					},
					success : function (data, textStatus, request) {

						window.rateLimit = Number(request.getResponseHeader('X-RateLimit-Remaining'));
						console.log(window.rateLimit);

						if (window.rateLimit < 550) {
							window.delay = 700e3;
							clearInterval(eventLoop);
							eventLoop = setInterval(getMatrixFunc, window.delay);
							console.log("recalled Interval window delay 600");
						} else if (window.delay == 700e3) {
							window.delay = 3000;
							clearInterval(eventLoop);
							eventLoop = setInterval(getMatrixFunc, window.delay);
							console.log("reset Interval window delay 10s");
						}

						console.log(window.delay);

						totalData = totalData + data + ", SEGMENT: " + api.segments[api.config.siteId][window.index].name + " - CAMPAIGN: " + campaignName + breakText;

						console.log(api.segments[api.config.siteId][window.index].name);
						window.inProgress = false;
						window.index = window.index + 1;

					}
				});
			}

			if (window.inProgress == false && window.index == window.api.segments[window.api.config.siteId].length) {
				clearInterval(eventLoop);
				window.index = 0;
				window.inProgress = false;
				window.inProgressAll = false;
				window.indexAll = window.indexAll + 1;

				api.reports[campaignId] = {
					"data" : totalData,
					"name" : campaignName
				};

				$(".active-loading").parents(2).next().find("a").attr("data", campaignId);

				$(".active-loading").parents().eq(1).next().find("a.download").removeClass("hidden");
				$(".active-loading").hide();
				$(".active-loading").parents(2).find("button").show();
				$(".active-loading").removeClass("active-loading");
			}
		}

		var eventLoop;

		eventLoop = setInterval(getMatrixFunc, window.delay);

	},
	"triggerDownload" : function (data, campaignName) {

		var downloadData = data;
		var finalString = campaignName.replace(/[^a-zA-Z0-9 -]+/gi, "");

		console.log(finalString);

		chrome.fileSystem.chooseEntry({
			type : "saveFile",
			suggestedName : "Site-" + api.config.siteId + "-" + finalString + ".csv"
		},
			function (writableFileEntry) {
			writableFileEntry.createWriter(function (writer) {
				writer.onwriteend = function (e) {
					if (window.downloadProgress == true) {
						console.log("finished write");
						window.downloadIndex = window.downloadIndex + 1;
						window.downloadProgress = false;
					}
				};
				writer.write(new Blob([downloadData], {
						type : 'CSV File'
					}));
			}, function (err) {
				console.log(err);
			});
		});
	}
}

$("#openFile").click(function () {
	api.getLocalPath(function (path) {
		console.log(path);
	});
});

$("#getCampaigns").click(function (e) {
	// Check that another request is not already in progress
	if (window.inProgress == false) {
		api.getToken(function (token) {
			api.getActiveCampaigns(token);
		});
	} else {
		console.log("already requesting data");
	}
});

$("a.dropdown-item").click(function () {

	console.log($(this).attr("value"));
	$(".active").removeClass("active");

	var name = $(this).text();
	$("button.dropdown-toggle").text(name);

	$(this).addClass("active");

	$("#downloadAll").hide();
});

$("a.active").click();

$("#showSegments").click(function () {

	if ($("#showSegments").hasClass("show")) {
		$(".segments").html("");
		$("#showSegments").text("Show Segments");
		$("#showSegments").removeClass("show");
	} else {

		var segments = api.segments[api.config.siteId];

		for (var i = 0; i < segments.length; i++) {
			$(".segments").append("<br>" + JSON.stringify(segments[i]));
		}

		$("#showSegments").text("Hide Segments");
		$("#showSegments").addClass("show");
	}
});

$("#requestAll").click(function () {
	api.getToken(function (token) {
		api.getMatrixAll(token);
	});
});

$("#downloadAll").click(function () {

	if (window.inProgress == false && window.inProgressAll == false) {

		var keys = Object.keys(api.reports);

		var reportsList = keys.length - 1;

		console.log(reportsList);
		console.log(keys);

		var eventLoopDownload = setInterval(function () {

				console.log(window.downloadIndex, reportsList);

				if (window.downloadProgress == false && window.downloadIndex <= reportsList) {
					window.downloadProgress = true;
					api.triggerDownload(api.reports[keys[window.downloadIndex]].data, api.reports[keys[window.downloadIndex]].name);
				}

				if (window.downloadIndex > reportsList && window.downloadProgress == false) {
					console.log("clear interval ran");
					window.downloadIndex = 0;
					clearInterval(eventLoopDownload);
				}
			}, 1e3);
	}
});
